# DeFork

DeFork is for you to easily and conveniently create different, interactive, intriguing, branched, nonlinear conversations using json data.
One of the popular tool for creating dialogs used as an example here is Twine - an open-source tool by Chris Klimas - see twinery.org for more information.
Example utilizes also RichText for a convenient and easy way of representing a dialog window - thanks to Bjorn Ritzl - see github.com/britzl/defold-richtext for more information.

With Defork you can easily:

- load json data exported from conversation making tool, such as Twine
- get text, options and links leading to next dialog's nodes
- store and manage current links, options and flow through nodes
- use RichText to easily show dialog window, modify text effects
- modify data by adding actors, colors and custom text effects
- add images, portraits or emoticons to Rich Text
- manage input and handle transitions between nodes

[Build and run](defold://build) to see an example in action. You can of course alter any settings to fit your needs.

If you run into trouble, help is available in [our forum](https://forum.defold.com).

Happy Deforking!

---